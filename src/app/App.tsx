import {  Container } from '@mui/material';
import AppProviders from './Providers';
import TasksWrapper from './components/task/TasksWrapper';

function App() {
  return (
      <Container sx={{backgroundColor: '#fff', minHeight: '100vh'}} >
        <TasksWrapper />
      </Container>
  );
}

export default AppProviders(App)();
