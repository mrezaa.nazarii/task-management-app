import { createTheme } from '@mui/material/styles';

// A custom theme for this app

export const PRIMARY_COLOR = '#F0BC42';
export const SECONDARY_COLOR = '#8E1F2F';

const theme = createTheme({
    palette: {
        primary: {
            main: PRIMARY_COLOR,
        },
        secondary: {
            main: SECONDARY_COLOR,
        },
        error: {
            main: '#ef233c'
        },
    },
});

export default theme;