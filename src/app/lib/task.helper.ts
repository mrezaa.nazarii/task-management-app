import { Task, TaskStatus } from "../models/task";

export const isExistTask = (tasks: Task[]) => tasks.length > 0;
export const isTasksListFiltered = (status: TaskStatus) => status !== 'all';
export const filterCompletedTasks = (tasks: Task[]) => tasks.filter(tasks => tasks.isCompleted);
export const filterNotCompletedTasks = (tasks: Task[]) => tasks.filter(tasks => !tasks.isCompleted);

export const filterTasksByStatus = (tasks: Task[], status: TaskStatus) => {
    switch (status) {
        case 'pending':
            return filterNotCompletedTasks(tasks)
        case 'completed':
            return filterCompletedTasks(tasks)         
        default:
            return tasks
    }
}