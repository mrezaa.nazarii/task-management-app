import { Typography } from '@mui/material'
import { memo } from 'react'

type Props = {}

const SortMsg = (props: Props) => {
  return (
    <Typography variant='body2' sx={{marginY: '0.5rem'}} >Tip: Drag & Drop to sort</Typography>
  )
}

export default memo(SortMsg)