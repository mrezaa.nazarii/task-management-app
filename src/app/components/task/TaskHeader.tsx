import { SxProps, Typography } from '@mui/material'
import { PRIMARY_COLOR, SECONDARY_COLOR } from '../../configs/material.theme'
import { memo } from 'react'

type Props = {
  title?: string
}

const headerStyle: SxProps = {
    padding: '1rem 2rem',
    marginBottom: '1rem',
    backgroundColor: PRIMARY_COLOR,
    color: SECONDARY_COLOR
}

const TaskHeader = ({ title = 'Tasks' }: Props) => {
  return (
    <Typography sx={headerStyle} variant='h4' >{title}</Typography>
  )
}

export default memo(TaskHeader)