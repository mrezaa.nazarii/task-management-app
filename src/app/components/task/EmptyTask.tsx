import { Box, SxProps, Typography } from '@mui/material'
import { memo } from 'react'

type Props = {}

const style: SxProps = {
    padding: '1rem', 
    border: '1px solid lightgray', 
    display: 'flex', 
    alignItems: 'center', 
    justifyContent: 'center',
    borderRadius: '5px'
}

const EmptyTask = (props: Props) => {
  return (
    <Box sx={style} ><Typography sx={{textAlign:'center'}} variant='h6'>There is no Task!</Typography></Box>
  )
}

export default memo(EmptyTask)