import { useDispatch } from 'react-redux';
import { changeStatus } from '../../store/task/task-slice';
import {Select, MenuItem, InputLabel, FormControl} from "@mui/material";
import { memo } from 'react'
import useTaskState from '../../hooks/useTaskState';

type Props = {}

const TasksFilter = (props: Props) => {
    const dispatch = useDispatch();
    const { status} = useTaskState();

    const onChange = (event: any) => {
      dispatch(changeStatus(event.target.value))
    }
    return (
      <FormControl fullWidth>
          <InputLabel id="status-drop-down">Status</InputLabel>  
          <Select
              value={status}
              onChange={onChange}         
              label='Status'
              labelId='status-drop-down'
          >
              <MenuItem value={'all'}>All</MenuItem>
              <MenuItem value={'pending'}>Not Completed</MenuItem>
              <MenuItem value={'completed'}>Completed</MenuItem>
          </Select>
      </FormControl>
    )
}

export default memo(TasksFilter)