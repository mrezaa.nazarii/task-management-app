import {  SyntheticEvent, useState } from 'react'
import Box from '@mui/material/Box';
import { useDispatch } from 'react-redux';
import { addTask } from '../../store/task/task-slice';
import { Button, SxProps, TextField } from '@mui/material';
import { memo } from 'react'

type Props = {}

const formStyle: SxProps =  {
  display: 'flex', 
  flexDirection: 'column',
  gap: '1rem', 
  padding: '1rem',
  border: '1px solid lightgray',
  marginBottom:' 2rem',
  borderRadius:'5px'
}

const TaskForm = (props: Props) => {
  const [title, setTitle] = useState<string>('');
  const [description, setDescription] = useState<string>('');
  const dispatch = useDispatch();
  
  const submitHandler =  (event: SyntheticEvent) => {
    event.preventDefault();

    dispatch(addTask({
      title,
      description
    }));

    resetForm();
  }
  const resetForm = () => {
    setTitle('');
    setDescription('');
  }

  const isDisabled = () => !title || !description;

  return (

      <form onSubmit={submitHandler}>
        <Box sx={formStyle} >
          <TextField 
            fullWidth 
            value={title}
            onChange={e => setTitle(e.target.value)}
            label='Title'
            />
          <TextField 
            fullWidth 
            value={description}
            onChange={e => setDescription(e.target.value)}
            label='Description'
            />
          <Button 
            type='submit' 
            variant="contained" 
            size='large'  
            disabled={isDisabled()}
            >
              Save
          </Button>
        </Box>
      </form>

  )
}

export default memo(TaskForm)