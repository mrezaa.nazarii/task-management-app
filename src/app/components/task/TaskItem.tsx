import { forwardRef } from 'react'
import { Task } from '../../models/task'
import { Typography, Box, Button, SxProps, Divider } from '@mui/material'
import {  SECONDARY_COLOR } from '../../configs/material.theme'
import { useDispatch } from 'react-redux'
import { toggleCompleteTask } from '../../store/task/task-slice'
import { memo } from 'react'

type Props = {
    task: Task
}

const cardStyle: SxProps = {
    padding: '1rem 2rem', 
    borderRadius: '5px',
    background: SECONDARY_COLOR,
    color: 'white'
}
const actionStyle: SxProps = { display: 'flex', gap: '1rem'};
const dividerStyle:SxProps = { backgroundColor: '#ffffff', marginY: '1rem'};
const headerStyle:SxProps = { display: 'flex', gap:'1', alignItems: 'center'};

const TaskItem = forwardRef((props: Props, ref: any) =>  {
  const {task} = props;
	const dispatch = useDispatch();

  const toggleCompleted = (id: string) => {
    dispatch(toggleCompleteTask(id))
  }

  const toggleButtonVariant = (isCompleted: boolean) => isCompleted ? 'outlined' : 'contained'

  return (
    <Box sx={cardStyle} ref={ref} {...props} >
      <Box sx={headerStyle} >
        <Typography sx={{flexGrow: '1', textDecoration : task.isCompleted ? 'line-through' : 'none'}} variant='h5'  gutterBottom>
          {task.title}
        </Typography>

        <Box sx={actionStyle} >
            <Button color='primary' 
              variant={toggleButtonVariant(task.isCompleted)} 
              onClick={() => toggleCompleted(task.id)} >
                { task.isCompleted ? 'Undo' : 'Done' }
            </Button>
        </Box>

      </Box>
      <Divider sx={dividerStyle}  />
      <Typography variant='body1' >{task.description}</Typography>
        
    </Box>
  )
})

export default memo(TaskItem)