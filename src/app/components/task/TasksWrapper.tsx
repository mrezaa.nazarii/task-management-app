
import TaskHeader from './TaskHeader';
import TaskForm from './TaskForm';
import TasksFilter from './TasksFilter';
import SortMsg from './SortMsg';
import TaskItems from './TaskItems';
import { Divider, Grid } from '@mui/material';
import useTaskState from '../../hooks/useTaskState';
import { isExistTask, isTasksListFiltered } from '../../lib/task.helper';

type Props = {}

const TasksWrapper = (props: Props) => {
    const { tasks, status } = useTaskState();
    
    return (
        <>
            <TaskHeader />
            <Grid container spacing={2} >
                <Grid item xs={12} lg={6} >
                    <TaskForm />        
                    <Divider sx={{marginY: '1rem'}} />
                    {isExistTask(tasks) ? <TasksFilter /> : null }
                    {isExistTask(tasks) && !isTasksListFiltered(status) ? <SortMsg />: null}
                </Grid>
                <Grid item xs={12} lg={6} >
                    <TaskItems />
                </Grid>
            </Grid>  
        </>
    )
}

export default TasksWrapper