import TaskItem from './TaskItem'
import { Box, SxProps } from '@mui/material'
import { useDispatch } from 'react-redux'
import { sortTasks } from '../../store/task/task-slice'
import { useEffect, useState} from 'react'
import { List, arrayMove } from 'react-movable'
import EmptyTask from './EmptyTask'
import useTaskState from '../../hooks/useTaskState'
import { filterTasksByStatus, isExistTask, isTasksListFiltered } from '../../lib/task.helper'

type Props = {}

const listStyle: SxProps = {
    display: 'flex',
    flexDirection: 'column',
    gap: '1rem',
    width: '100%',
    cursor: 'pointer'
}

const TaskItems = (props: Props) => {
    const { tasks, status} = useTaskState();
    const [filteredTasks, setFilteredTasks] = useState(tasks);
    const dispatch = useDispatch();

    useEffect(() => {
        setFilteredTasks(filterTasksByStatus(tasks, status))
    }, [tasks, status])

    if(!isExistTask(filteredTasks)) {
        return <EmptyTask />
    }
    
    if(isTasksListFiltered(status)){
        return(
            <Box sx={listStyle}>
                {filteredTasks?.map(task => <TaskItem key={task.id} task={task}/>)}
            </Box>
        )
    }

    return (
        <List
            values={filteredTasks}            
            onChange={({ oldIndex, newIndex }) =>
                {
                    setFilteredTasks(arrayMove(filteredTasks, oldIndex, newIndex))
                    dispatch(sortTasks(arrayMove(filteredTasks, oldIndex, newIndex)))                    
                }
            }
            renderList={({ children, props }) => <Box sx={listStyle} {...props} >{children}</Box>}
            renderItem={({ value: task, props }) => <TaskItem key={task.id} task={task} {...props} /> }
        />
    )
}

export default TaskItems