
import * as React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import { ThemeProvider } from '@mui/material/styles';
import theme from './configs/material.theme';
import { Provider } from 'react-redux';
import store from './store';
import { PersistGate } from 'redux-persist/integration/react'
import { persistStore } from 'redux-persist';

type Props = {}

let persister = persistStore(store)

const AppProviders = (Component: React.FunctionComponent) => (props?: Props) => {
    const WrapperComponent = () => (
        <>
            <Provider store={store}>
                <PersistGate loading={null} persistor={persister}>
                    <ThemeProvider theme={theme} >
                        <CssBaseline />
                        <Component {...props} />
                    </ThemeProvider>
                </PersistGate>
            </Provider>
        </>
    );

    return WrapperComponent;
};

export default AppProviders;