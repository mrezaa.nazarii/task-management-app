export type Task = {
    id: string;
    title: string;
    description: string;
    isCompleted: boolean;
}

export type AddTaskRequest = Pick<Task, 'description' | 'title'>;

export type TaskStatus = 'completed' | 'pending' | 'all';

export type TaskState = {
    tasks: Task[],
    status: TaskStatus,
}
