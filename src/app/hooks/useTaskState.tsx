import { useSelector } from "react-redux";
import { TaskState } from "../models/task";
import { RootState } from "../store";

export default function useTaskState(): TaskState {
    const slice: TaskState = useSelector<RootState, any>((state: RootState)=> state.task);

    return slice;
  }