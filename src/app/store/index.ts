import { configureStore, ThunkAction, Action, ThunkMiddleware } from '@reduxjs/toolkit';
import persistedReducer from './rootReducer';

const middleware: ThunkMiddleware[] = [];

if (process.env.NODE_ENV === 'development') {
    const { createLogger } = require(`redux-logger`);
    const logger = createLogger({ collapsed: (getState: any, action: any, logEntry: any) => !logEntry.error });
  
    middleware.push(logger);
}
  
const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      immutableCheck: false,
      serializableCheck: false,
    }).concat(middleware),
  devTools: process.env.NODE_ENV === 'development',
});


export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;

export default store;
