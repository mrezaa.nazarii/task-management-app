import { combineReducers } from '@reduxjs/toolkit';
import taskSlice from './task/task-slice'
import storage from 'redux-persist/lib/storage' //
import {  persistReducer } from 'redux-persist'


const persistConfig = {
  key: 'root',
  storage,
}

const reducer = combineReducers({
  task: taskSlice
})

const persistedReducer = persistReducer(persistConfig, reducer) 
export default persistedReducer;
