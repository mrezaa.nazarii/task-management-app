import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { AddTaskRequest, Task, TaskState, TaskStatus } from "../../models/task";

const TASK = 'task'

const initialState: TaskState = {
    tasks: [],
    status: 'all',
}

const taskSlice = createSlice({
    name: TASK,
    initialState,
    reducers: {   
        addTask:(state, action: PayloadAction<AddTaskRequest>) => {
            const newTask: Task = {
                ...action.payload,
                id: (Math.random() + 1).toString(16).substring(7),
                isCompleted: false
            }
            state.tasks.push(newTask);
        },
        toggleCompleteTask:(state, action: PayloadAction<string>) => {
            const findIndex = state.tasks.findIndex(task => task.id === action.payload);
            let copiedTasks = [...state.tasks];

            copiedTasks[findIndex] = {
                ...copiedTasks[findIndex],
                isCompleted: !copiedTasks[findIndex].isCompleted
            }

            state.tasks = copiedTasks;
        },
        changeStatus: (state, action:PayloadAction<TaskStatus>) => {
            state.status = action.payload
        },
        sortTasks:(state, action: PayloadAction<Task[]>) => {
            state.tasks = action.payload;
        },
    },
});

export const { toggleCompleteTask, changeStatus, addTask, sortTasks } = taskSlice.actions;

export default taskSlice.reducer;